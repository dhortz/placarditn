var chalk = require('chalk');
var green = chalk.green.bold;
var print = console.log;

var i;
var text = "";
var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

for(i = 0; i < 1000; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    if(i%2==0){
        print(green(text));
        print('\t \t \t')
        print(green(text));
        print('\t')
    }
    print(green(text));
    print('\t');
}
