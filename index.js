'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _superagent = require('superagent');

var _superagent2 = _interopRequireDefault(_superagent);

var _googlespreadsheets = require('google-spreadsheets');

var _googlespreadsheets = _interopRequireDefault(_googlespreadsheets);

var _package = require('./package.json');

var pkg = _interopRequireWildcard(_package);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var USER_AGENT = pkg.name + '/' + pkg.version;
var API_KEY = '552CF226909890A044483CECF8196792';
var CHANNEL = '1';

var makeRequest = function makeRequest(options) {
  return new _bluebird2.default(function (resolve, reject) {
    _superagent2.default.get('https://www.jogossantacasa.pt/' + options.path).query(_extends({
      apiKey: API_KEY,
      channel: CHANNEL
    }, options.query)).set('If-Modified-Since', (0, _moment2.default)().format('ddd, D MMM YYYY HH:mm:ss [GMT]Z')).set('User-Agent', USER_AGENT).end(function (err, res) {
      if (err) return reject(err);

      var data = res.body,
          header = data.header;

      if (header.responseSuccess === false) {
        return reject(new Error(header.errorCode + ' - ' + header.errorMessage));
      }

      resolve(data.body.data);
    });
  });
};

var placard = {
  fullSportsBook: function fullSportsBook(cb) {
    return makeRequest({
      path: '/WebServices/SBRetailWS/FullSportsBook'
    }).nodeify(cb);
  },
  nextEvents: function nextEvents(cb) {
    return makeRequest({
      path: '/WebServices/SBRetailWS/NextEvents'
    }).nodeify(cb);
  },
  info: function info(cb) {
    return makeRequest({
      path: '/WebServices/ContentWS/Contents/',
      query: { categoryCode: 'ADRETAILINFOS' }
    }).nodeify(cb);
  },
  faq: function faq(cb) {
    return makeRequest({
      path: '/WebServices/ContentWS/Contents/',
      query: { categoryCode: 'ADRETAILFAQSAPP' }
    }).nodeify(cb);
  }
};

module.exports = placard;
