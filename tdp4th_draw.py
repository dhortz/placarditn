#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import time

teams_to_be_drawn = ['Covilhã', 'Leixões', 'Sporting', 'Guimarães',
                     'Torreense', 'Tondela', 'Braga', 'Benfica',
                     'Estoril', 'Académica', 'Vilafranquense', 'Real',
                     'Sanjoanense', 'Chaves', 'Penafiel', 'Setúbal']

benfica_draws = {'Covilhã': 0, 'Leixões': 0, 'Sporting': 0, 'Guimarães': 0,
                 'Torreense': 0, 'Tondela': 0, 'Braga': 0, 'Estoril': 0,
                 'Académica': 0, 'Vilafranquense': 0, 'Real': 0, 'Sanjoanense': 0,
                 'Chaves': 0, 'Penafiel': 0, 'Setúbal': 0}

home = 0
away = 0

for i in range(0, 100):
    print("Simulation no." + str(i+1))
    teams = ['Covilhã', 'Leixões', 'Sporting', 'Guimarães',
             'Torreense', 'Tondela', 'Braga', 'Benfica',
             'Estoril', 'Académica', 'Vilafranquense', 'Real',
             'Sanjoanense', 'Chaves', 'Penafiel', 'Setúbal']

    drawnteams = []

    n = 0

    while len(teams)>0:
        random.shuffle(teams)
        i = random.randint(0, len(teams)-1)
        if(n % 2 != 0):
            if(drawnteams[n-1]=='Benfica'):
                li = list(range(len(teams)-1))
                while(teams[i] not in ['Guimarães', 'Tondela', 'Braga', 'Estoril', 'Chaves', 'Setúbal']):
                    print(li)
                    i = random.choice(li)
                    print(i)
                    if(teams[i] not in ['Guimarães', 'Tondela', 'Braga', 'Estoril', 'Chaves', 'Setúbal']):
                        li.remove(i)
                drawnteams += [teams[i]]
            else:
                drawnteams += [teams[i]]
        else:
            drawnteams += [teams[i]]
        n += 1
        teams.remove(teams[i])

    matches = { 'j1': [drawnteams[0], drawnteams[1]], 'j2': [drawnteams[2], drawnteams[3]],
                'j3': [drawnteams[4], drawnteams[5]], 'j4': [drawnteams[6], drawnteams[7]],
                'j5': [drawnteams[8], drawnteams[9]], 'j6': [drawnteams[10], drawnteams[11]],
                'j7': [drawnteams[12], drawnteams[13]], 'j8': [drawnteams[14], drawnteams[15]]}

    for m in matches:
        if(matches[m][0] == 'Benfica'):
            home += 1
            benfica_draws[matches[m][1]] += 1
        if(matches[m][1] == 'Benfica'):
            away += 1
            benfica_draws[matches[m][0]] += 1

# print("\n------------------------------------------------------\n")
# for m in matches:
#     print(matches[m][0] + " vs " + matches[m][1])


# print("\n------------------------------------------------------\n")
# for d in benfica_draws:
#     print(d + ": " + str(benfica_draws[d]))


# print("\n------------------------------------------------------\n")
# print("Home: " + str(home))
# print("Away: " + str(away))
