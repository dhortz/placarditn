#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

pot1 = [{'team': 'Arsenal', 'country': 'ing', 'group': 'a'},
        {'team': 'Napoli', 'country': 'ita', 'group': 'b'},
        {'team': 'Barcelona', 'country': 'esp', 'group': 'c'},
        {'team': 'Atl Madrid', 'country': 'esp', 'group': 'd'},
        {'team': 'Monaco', 'country': 'fra', 'group': 'e'},
        {'team': 'Dortmund', 'country': 'ale', 'group': 'f'},
        {'team': 'Leicester', 'country': 'ing', 'group': 'g'},
        {'team': 'Juventus', 'country': 'ita', 'group': 'h'}]

pot2 = [{'team': 'PSG', 'country': 'fra', 'group': 'a'},
        {'team': 'Benfica', 'country': 'por', 'group': 'b'},
        {'team': 'Man City', 'country': 'ing', 'group': 'c'},
        {'team': 'Bayern Munich', 'country': 'ale', 'group': 'd'},
        {'team': 'Bayer Leverkusen', 'country': 'ale', 'group': 'e'},
        {'team': 'Real Madrid', 'country': 'esp', 'group': 'f'},
        {'team': 'Porto', 'country': 'por', 'group': 'g'},
        {'team': 'Sevilla', 'country': 'esp', 'group': 'h'}]

drawnteams = []

benfica_draws = {'Arsenal': 0, 'Barcelona': 0, 'Atl Madrid': 0, 'Juventus': 0,
                 'Monaco': 0, 'Dortmund': 0, 'Leicester': 0}

count = 0

for i in range(0, 1000000):
    # print(i)
    fail = False
    while len(drawnteams)<16:
        n = 0
        random.shuffle(pot2)
        p2 = pot2[0]
        while p2 in drawnteams:
            random.shuffle(pot2)
            p2 = pot2[0]
        drawnteams += [p2] # drawn first team

        random.shuffle(pot1)
        p1 = pot1[0]
        while (p1 in drawnteams or p1['country']==p2['country'] or p1['group']==p2['group']) and n<3:
            n += 1
            # print(n)
            random.shuffle(pot1)
            p1 = pot1[0]
        drawnteams += [p1]

        if n==3:
            fail = True

        # print("(" + pot2[p2]['group'] + ") " + pot2[p2]['team'] + " (" + pot2[p2]['country'] + ") - ("
        #         + pot1[p1]['group'] + ") " + pot1[p1]['team'] + " (" + pot1[p1]['country'] + ")")


    matches = { 'j1': [drawnteams[0], drawnteams[1]], 'j2': [drawnteams[2], drawnteams[3]],
                'j3': [drawnteams[4], drawnteams[5]], 'j4': [drawnteams[6], drawnteams[7]],
                'j5': [drawnteams[8], drawnteams[9]], 'j6': [drawnteams[10], drawnteams[11]],
                'j7': [drawnteams[12], drawnteams[13]], 'j8': [drawnteams[14], drawnteams[15]]}

    for m in matches:
        if(matches[m][0]['team']=='Benfica') and fail == False:
            benfica_draws[matches[m][1]['team']] += 1
            count += 1
        # print(matches[m][0]['team'] + " vs. " + matches[m][1]['team'] + " (" + matches[m][0]['group'], matches[m][1]['group'] + ")" + " (" + matches[m][0]['country'], matches[m][1]['country'] + ")")

    drawnteams = []

for b in benfica_draws:
    print(b + ": " + str(round((benfica_draws[b]/count)*100.0, 2)) + "%")
