#!/usr/bin/env python
# encoding: utf-8

import time
import sys
import json
import colorama
from colorama import Fore, Back, init, Style
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *
import gspread
from oauth2client.service_account import ServiceAccountCredentials

class PlacardParser:

    def __init__(self):
        self.Foot_ALL = []
        self.Foot_All_Org = {}
        self.odds = None
        self.winrate = {}

        colorama.init()

        # country ID's
        self.country_id = {"int": "23316", "pt": "23319", "es": "23332", "en": "23350",
                    "de": "23365", "fr": "23331", "it": "23347", "nl": "23366",
                    "be": "23335", "ch": "23364", "ua": "23362"}

        # load odds from file
        with open('odds.json', 'r') as fp:
            self.odds = json.load(fp)

        # get win rates from excel
        self.excel_parser()

        # END OF __INIT__

    #########################
    #                       #
    #       FUNCTIONS       #
    #                       #
    #########################

    # update odds
    def update_odds(self):
        # connect to browser
        driver = self.establish_connection()

        # navigation
        self.navigation(driver, self.country_id['int']) # Internacional (Champions e Liga Europa)
        self.navigation(driver, self.country_id['pt']) # Portugal
        self.navigation(driver, self.country_id['de']) # Alemanha
        self.navigation(driver, self.country_id['be']) # Bélgica
        self.navigation(driver, self.country_id['es']) # Espanha
        self.navigation(driver, self.country_id['fr']) # França
        self.navigation(driver, self.country_id['nl']) # Holanda
        self.navigation(driver, self.country_id['en']) # Inglaterra
        self.navigation(driver, self.country_id['it']) # Itália
        self.navigation(driver, self.country_id['ch']) # Suíça
        self.navigation(driver, self.country_id['ua']) # Ucrânia

        # close browser
        driver.quit()

        # get all the values in the right place with the right key
        for ind, elem in enumerate(self.Foot_ALL):
            if(ind%2==0):
                self.Foot_All_Org[elem] = {} # use name of league as key
            else:
                self.Foot_All_Org[self.Foot_ALL[ind-1]] = elem # odds from league

        # write odds to file
        with open('odds.json', 'w') as fp:
            json.dump(self.Foot_All_Org, fp)

    # establish connection
    def establish_connection(self):
        driver = webdriver.Firefox()
        driver.minimize_window()
        driver.get("https://www.jogossantacasa.pt/web/Placard/placard")
        return driver

    # navigating through the page
    def navigation(self, driver, country_id):
        elem = None
        try:
            elem = driver.find_element_by_xpath("//a[@class='first foot_2']")
        except:
            try:
                elem = driver.find_element_by_xpath("//a[@class='first foot_2 selected']")
            except:
                try:
                    elem = driver.find_element_by_xpath("//a[@class='first foot_2 active']")
                except:
                    elem = driver.find_element_by_xpath("//a[@class='first foot_2 selected active']")
        hov = ActionChains(driver).move_to_element(elem)
        hov.perform()
        time.sleep(1)
        try:
            pt = driver.find_element_by_xpath("//a[@href='/web/Placard/eventos?id=" + country_id + "']")
        except:
            return
        pt.click()
        time.sleep(1)
        section = driver.find_element_by_xpath("//div[@class='section']")
        divs = section.find_elements_by_xpath("./div")
        for d in divs:
            try:
                self.Foot_ALL.append(d.find_element_by_xpath("./h2").text)
            except NoSuchElementException:
                self.Foot_ALL.append(self.iterateContentCompetition(d))

    # iteration through the various competitions in the specified country
    def iterateContentCompetition(self, elem):
        contComp = {}
        elem = elem.find_elements_by_xpath("./div")
        for ind, e in enumerate(elem):
            try:
                if(e.find_element_by_xpath("./h3").text=="1X2 TR"):
                    contComp[e.find_element_by_xpath("./h3").text] = {}
                if(e.find_element_by_xpath("./h3").text=="1X2 INT"):
                    contComp[e.find_element_by_xpath("./h3").text] = {}
                if(e.find_element_by_xpath("./h3").text=="1X2 DV"):
                    contComp[e.find_element_by_xpath("./h3").text] = {}
                if(e.find_element_by_xpath("./h3").text=="Mais/Menos"):
                    contComp[e.find_element_by_xpath("./h3").text] = {}
            except NoSuchElementException:
                if(ind%2==1):
                    contComp[elem[ind-1].text] = self.iterateTable(e)
        return contComp

    # iteration through the items of the table (team and odds)
    def iterateTable(self, elem):
        contTable = {}
        contDate = elem.find_elements_by_xpath("./div")
        for cd in contDate:
            tr = cd.find_elements_by_xpath("./table/tbody/tr")
            for e in tr:
                try:
                    contTable[e.find_element_by_class_name("marketIndex").text] = {}
                    td = e.find_elements_by_xpath("./td/div")
                    for ind, t in enumerate(td):
                        if "Mais" not in t.find_element_by_xpath("./div").text and "Menos" not in t.find_element_by_xpath("./div").text:
                            if ind==0:
                                contTable[e.find_element_by_class_name("marketIndex").text][t.find_element_by_xpath("./div").text] = {"side": "H", "odd": t.find_element_by_xpath("./div/span").text.replace(",", ".")}
                            elif ind==2:
                                contTable[e.find_element_by_class_name("marketIndex").text][t.find_element_by_xpath("./div").text] = {"side": "A", "odd": t.find_element_by_xpath("./div/span").text.replace(",", ".")}
                            else:
                                contTable[e.find_element_by_class_name("marketIndex").text][t.find_element_by_xpath("./div").text] = {"side": "N", "odd": t.find_element_by_xpath("./div/span").text.replace(",", ".")}
                        else:
                            contTable[e.find_element_by_class_name("marketIndex").text][t.find_element_by_xpath("./div").text] = {"odd": t.find_element_by_xpath("./div/span").text.replace(",", ".")}
                except NoSuchElementException:
                    print "error"
        return contTable

    # get odds
    def get_odds(self):
        return self.odds

    # print odds in fashion
    def print_odds(self):
        i = 0
        n = 0
        for league, all_matches in self.odds.items():
            for match_type, matches in all_matches.items():
                for code, winner in matches.items():
                    i += 1
                    print Fore.GREEN + code + " - " + match_type + " - " + str(winner)
                    time.sleep(0.05)
                    if(i%10==0):
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[F")
                        sys.stdout.write("\033[K")

        if(i%10==0):
            print "\n"
        elif(i%7==0):
            print "\n\n\n"
        elif(i%5==0):
            print "\n\n\n\n\n"
        elif(i%3==0):
            print "\n\n\n\n\n\n\n"
        elif(i%2==0):
            print "\n\n\n\n\n\n\n\n"

    # get win rate from excel
    def excel_parser(self):
        # use creds to create a client to interact with the Google Drive API
        scope = ['https://spreadsheets.google.com/feeds']
        creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
        client = gspread.authorize(creds)

        spreadsheet = client.open("Placard 2017/2018")

        worksheet = spreadsheet.worksheet("Winrate")

        wr = worksheet.get_all_values()[1:]

        # saving win rate
        for r in wr:
            r = filter(None, r)
            self.winrate[r[0]] = r[3:]

        # writing to file
        with open('winrate.json', 'w') as fp:
            json.dump(self.winrate, fp)

    # search code by name
    def search_code_by_team(self, team):
        for league, all_odds in self.odds.items():
            for t, all_matches in all_odds.items():
                for code, winner in all_matches.items():
                    for w, odd in winner.items():
                        if w==team:
                            return code

    # search match by name
    def search_match_by_name(self, team):
        code = self.search_code_by_team(team)
        match = {}
        match = {'code': code}
        for league, all_matches in self.odds.items():
            for t, matches in all_matches.items():
                for c, winners in matches.items():
                    if c==code:
                        match[t] = winners

        return match

    # get win rate by name
    def get_win_rate_by_name(self, team):
        no_matches = 0
        wins = 0
        wr = 0
        for t, s in self.winrate.items():
            if t == team:
                for m in s:
                    no_matches += 1
                    if m=="1":
                        wins += 1

        wr = int(round((wins/float(no_matches))*100.0))
        return wr

    # update win rate by name
    def update_win_rate_by_name(self, team, win):
        for t, wr in self.winrate.items():
            if t==team:
                if win==0:
                    self.winrate[t].append("0")
                elif win==1:
                    self.winrate[t].append("1")

        # write to file
        with open('winrate.json', 'w') as fp:
            json.dump(self.winrate, fp)


# start
if __name__ == "__main__":
    init(autoreset=True)
    p = PlacardParser()

    print "\ndone\n"
