var placard = require('./index.js');
var winrate = require('./winrate.json');
var chalk = require('chalk');
var liner = chalk.bgCyan.black;
var header = chalk.bgRed.white.bold;
var result = chalk.bgGreen.white.bold;
var line = "------------------------------------------------";
var print = console.log;
var teams = [
    {'team': "Sp. Braga", 'draw': 0, 'winrate': 0},
    {'team': "SL Benfica", 'draw': 0, 'winrate': 0},
    {'team': "Barcelona", 'draw': 0, 'winrate': 0},
    // {'team': "Valência", 'draw': 0, 'winrate': 0},
    // {'team': "Arsenal", 'draw': 0, 'winrate': 0},
    // {'team': "Manchester Utd.", 'draw': 0, 'winrate': 0},
    {'team': "Manchester City", 'draw': 0, 'winrate': 0},
    {'team': "Juventus", 'draw': 0, 'winrate': 0},
    // {'team': "Bayern Munique", 'draw': 0, 'winrate': 0},
    // {'team': "Bor. Dortmund", 'draw': 0, 'winrate': 0},
    // {'team': "Mónaco", 'draw': 0, 'winrate': 0},
    // {'team': "PSG", 'draw': 0, 'winrate': 0},
    // {'team': "Sporting CP", 'draw': 0, 'winrate': 0},
    // {'team': "FC Porto", 'draw': 0, 'winrate': 0},
    // {'team': "Shakhtar D.", 'draw': 0, 'winrate': 0},
    {'team': "Rosenborg", 'draw': 0, 'winrate': 0},
    // {'team': "PSV Eindhoven", 'draw': 0, 'winrate': 0},
    // {'team': "Young Boys", 'draw': 0, 'winrate': 0},
    // {'team': "Club Brugge", 'draw': 0, 'winrate': 0},
    // {'team': "HJK Helsínquia", 'draw': 0, 'winrate': 0},
    // {'team': "Toronto FC", 'draw': 0, 'winrate': 0},
    // {'team': "Tottenham", 'draw': 0, 'winrate': 0},
    // {'team': "Mónaco", 'draw': 0, 'winrate': 0},
]

res = placard.nextEvents((err, data)=> {
    if(process.argv.length<4){
        print(header("Usage: node placard.js minWinRate bet"))
        return;
    }
    var i
    print(header("Chosen teams (Win Rate):") + " ")
    for (i = 0; i<teams.length; i++){
        teams[i].winrate = winrateByTeam(winrate,teams[i].team)
        print(teams[i].team + " (" + teams[i].winrate + "%)")
    }
    print(liner(line))
    minWR = process.argv[2]
    bet = process.argv[3]
    print(header("Combo Calculator (Min Win Rate: " + minWR + "%):") + " ")
    print(line)
    comboCalc = comboCalculator(data, winrate, teams, minWR)
    print(result("Result:") + " " + comboCalc)
    print(result("Multiplier (" + bet + "€):") + " " + (comboCalc*bet).toFixed(2))
    print(liner(line))
});

function searchByTeam(data, team){
    var i, len = data['exportedProgrammeEntries'].length
    for(i = 0; i < len; i++){
        if((data['exportedProgrammeEntries'][i]['homeOpponentDescription']==team || data['exportedProgrammeEntries'][i]['awayOpponentDescription']==team) && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            return data['exportedProgrammeEntries'][i]
        }
    }
}

function searchByCode(data, code){
    var i, len = data['exportedProgrammeEntries'].length
    for(i = 0; i < len; i++){
        if(data['exportedProgrammeEntries'][i]['index']==code && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            return data['exportedProgrammeEntries'][i]
        }
    }
}

function searchTeamOdd(data, team, draw){
    var i, len = data['exportedProgrammeEntries'].length
    for(i = 0; i < len; i++){
        if(data['exportedProgrammeEntries'][i]['homeOpponentDescription']==team && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            if(draw == 1){
                return data['exportedProgrammeEntries'][i]['markets'][0]['outcomes'][1]['price']['decimalPrice']
            }
            if(data['exportedProgrammeEntries'][i]['markets'][0]['outcomes'][0]['outcomeDescription']==team){
                return data['exportedProgrammeEntries'][i]['markets'][0]['outcomes'][0]['price']['decimalPrice']
            }
        }
        if(data['exportedProgrammeEntries'][i]['awayOpponentDescription']==team && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            if(draw == 1){
                return data['exportedProgrammeEntries'][i]['markets'][0]['outcomes'][1]['price']['decimalPrice']
            }
            if(data['exportedProgrammeEntries'][i]['markets'][0]['outcomes'][2]['outcomeDescription']==team){
                return data['exportedProgrammeEntries'][i]['markets'][0]['outcomes'][2]['price']['decimalPrice']
            }
        }
    }

    return 1.0;
}

function comboCalculator(data, winrate, teams, minWR){
    var i, combo = 1.0
    for (i = 0; i<teams.length; i++){
        code = searchCodeByTeam(data, teams[i].team)
        homeOrAway = searchHomeAway(data, teams[i].team, teams[i].draw)
        wr = winrateByTeam(winrate, teams[i].team)
        odd = searchTeamOdd(data, teams[i].team, teams[i].draw)
        if(winrateByTeam(winrate, teams[i].team) >= minWR){
            combo *= parseFloat(searchTeamOdd(data, teams[i].team, teams[i].draw));
            if(code!=undefined && homeOrAway!=undefined && wr!=undefined && odd!=undefined){
                print(code + " " + homeOrAway + " " + teams[i].team + " (" + wr + "%): " + odd)
            }
        }
    }

    print(line);
    return combo.toFixed(2);
}

function winrateByTeam(winrate, team){
    try {
        return parseInt(winrate[team][0],10);
    } catch (e) {
        return 0;
    }

    return 0;
}

function comboWinRate(winrate, teams)
{
    var i, wrc = 0;
    for(i = 0; i < teams.length; i++){
        print(teams[i].team + ': ' + winrateByTeam(winrate, teams[i].team) + "%")
        wrc += winrateByTeam(winrate, teams[i].team)
    }

    print(line);
    return (wrc/teams.length).toFixed(2);
}

function searchCodeByTeam(data, team){
    var i, len = data['exportedProgrammeEntries'].length;
    for(i = 0; i < len; i++){
        if((data['exportedProgrammeEntries'][i]['homeOpponentDescription']==team || data['exportedProgrammeEntries'][i]['awayOpponentDescription']==team) && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            return data['exportedProgrammeEntries'][i]['index'];
        }
    }
}

function searchHomeAway(data, team, draw){
    var i, len = data['exportedProgrammeEntries'].length;
    for(i = 0; i < len; i++){
        if(data['exportedProgrammeEntries'][i]['homeOpponentDescription']==team && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            if(draw == 1){
                return 'X'
            }
            return 1;
        }
        if(data['exportedProgrammeEntries'][i]['awayOpponentDescription']==team && data['exportedProgrammeEntries'][i]['sportCode']=='FOOT'){
            if(draw == 1){
                return 'X'
            }
            return 2;
        }
    }
}
